2020-08-22 14:41:24.693729: I tensorflow/core/platform/cpu_feature_guard.cc:141] Your CPU supports instructions that this TensorFlow binary was not compiled to use: AVX2 FMA
2020-08-22 14:41:24.789797: I tensorflow/stream_executor/cuda/cuda_gpu_executor.cc:897] successful NUMA node read from SysFS had negative value (-1), but there must be at least one NUMA node, so returning NUMA node zero
2020-08-22 14:41:24.790199: I tensorflow/core/common_runtime/gpu/gpu_device.cc:1405] Found device 0 with properties: 
name: GeForce GTX 1080 Ti major: 6 minor: 1 memoryClockRate(GHz): 1.582
pciBusID: 0000:01:00.0
totalMemory: 10.91GiB freeMemory: 10.73GiB
2020-08-22 14:41:24.790211: I tensorflow/core/common_runtime/gpu/gpu_device.cc:1484] Adding visible gpu devices: 0
2020-08-22 14:41:24.972595: I tensorflow/core/common_runtime/gpu/gpu_device.cc:965] Device interconnect StreamExecutor with strength 1 edge matrix:
2020-08-22 14:41:24.972625: I tensorflow/core/common_runtime/gpu/gpu_device.cc:971]      0 
2020-08-22 14:41:24.972631: I tensorflow/core/common_runtime/gpu/gpu_device.cc:984] 0:   N 
2020-08-22 14:41:24.972776: I tensorflow/core/common_runtime/gpu/gpu_device.cc:1097] Created TensorFlow device (/job:localhost/replica:0/task:0/device:GPU:0 with 10380 MB memory) -> physical GPU (device: 0, name: GeForce GTX 1080 Ti, pci bus id: 0000:01:00.0, compute capability: 6.1)
2020-08-22 14:41:25.540393: I tensorflow/core/kernels/cuda_solvers.cc:159] Creating CudaSolver handles for stream 0x7f210a8a0080


Formato dos dados:
X Treino:  (154419, 20, 4)
X Teste:  (51459, 20, 4)
Y Treino:  (154419, 1)
Y Teste:  (51459, 1)
_________________________________________________________________
Layer (type)                 Output Shape              Param #   
=================================================================
bidirectional (Bidirectional (None, 20, 200)           84000     
_________________________________________________________________
dropout (Dropout)            (None, 20, 200)           0         
_________________________________________________________________
bidirectional_1 (Bidirection (None, 20, 100)           100400    
_________________________________________________________________
dropout_1 (Dropout)          (None, 20, 100)           0         
_________________________________________________________________
bidirectional_2 (Bidirection (None, 20, 60)            31440     
_________________________________________________________________
dropout_2 (Dropout)          (None, 20, 60)            0         
_________________________________________________________________
bidirectional_3 (Bidirection (None, 20)                5680      
_________________________________________________________________
dropout_3 (Dropout)          (None, 20)                0         
_________________________________________________________________
dense (Dense)                (None, 1)                 21        
=================================================================
Total params: 221,541
Trainable params: 221,541
Non-trainable params: 0
_________________________________________________________________
None
STEPS:  20
FORECAST:  1
Epoch 1/12
 - 576s - loss: 0.0025
Epoch 2/12
 - 574s - loss: 0.0014
Epoch 3/12
 - 575s - loss: 0.0012
Epoch 4/12
 - 574s - loss: 0.0011
Epoch 5/12
 - 574s - loss: 0.0011
Epoch 6/12
 - 574s - loss: 0.0011
Epoch 7/12
 - 574s - loss: 0.0010
Epoch 8/12
 - 574s - loss: 0.0010
Epoch 9/12
 - 575s - loss: 0.0010
Epoch 10/12
 - 574s - loss: 9.8592e-04
Epoch 11/12
 - 574s - loss: 9.7217e-04
Epoch 12/12
 - 574s - loss: 9.5635e-04
MODELO SALVO
Nome do Modelo:  Modelos_Tensao/modelo_bidirecional_radiacao_wavelet_multvariado_camada_3_lendo_20_predicao_1

ERROS:
Score Variance: 0.997 
Erro Absoluto Médio: 18.438 
Erro Médio Quadrático: 623.714 
MAPE: 95912081.250 
RMSE: 24.974 
Erro Mediano Absoluto: 15.500 
Erro R2: 0.995


Formato dos dados:
X Treino:  (154379, 60, 4)
X Teste:  (51419, 60, 4)
Y Treino:  (154379, 1)
Y Teste:  (51419, 1)
_________________________________________________________________
Layer (type)                 Output Shape              Param #   
=================================================================
bidirectional_4 (Bidirection (None, 60, 200)           84000     
_________________________________________________________________
dropout_4 (Dropout)          (None, 60, 200)           0         
_________________________________________________________________
bidirectional_5 (Bidirection (None, 60, 100)           100400    
_________________________________________________________________
dropout_5 (Dropout)          (None, 60, 100)           0         
_________________________________________________________________
bidirectional_6 (Bidirection (None, 60, 60)            31440     
_________________________________________________________________
dropout_6 (Dropout)          (None, 60, 60)            0         
_________________________________________________________________
bidirectional_7 (Bidirection (None, 20)                5680      
_________________________________________________________________
dropout_7 (Dropout)          (None, 20)                0         
_________________________________________________________________
dense_1 (Dense)              (None, 1)                 21        
=================================================================
Total params: 221,541
Trainable params: 221,541
Non-trainable params: 0
_________________________________________________________________
None
STEPS:  60
FORECAST:  1
Epoch 1/12
 - 1796s - loss: 0.0028
Epoch 2/12
 - 1792s - loss: 0.0015
Epoch 3/12
 - 1792s - loss: 0.0014
Epoch 4/12
 - 1792s - loss: 0.0013
Epoch 5/12
 - 1791s - loss: 0.0012
Epoch 6/12
 - 1793s - loss: 0.0011
Epoch 7/12
 - 1791s - loss: 0.0011
Epoch 8/12
 - 1792s - loss: 0.0011
Epoch 9/12
 - 1791s - loss: 0.0010
Epoch 10/12
 - 1792s - loss: 0.0010
Epoch 11/12
 - 1792s - loss: 0.0010
Epoch 12/12
 - 1793s - loss: 9.8285e-04
MODELO SALVO
Nome do Modelo:  Modelos_Tensao/modelo_bidirecional_radiacao_wavelet_multvariado_camada_3_lendo_60_predicao_1

ERROS:
Score Variance: 0.996 
Erro Absoluto Médio: 13.016 
Erro Médio Quadrático: 469.976 
MAPE: 10877730.469 
RMSE: 21.679 
Erro Mediano Absoluto: 8.460 
Erro R2: 0.996


Formato dos dados:
X Treino:  (154339, 100, 4)
X Teste:  (51379, 100, 4)
Y Treino:  (154339, 1)
Y Teste:  (51379, 1)
_________________________________________________________________
Layer (type)                 Output Shape              Param #   
=================================================================
bidirectional_8 (Bidirection (None, 100, 200)          84000     
_________________________________________________________________
dropout_8 (Dropout)          (None, 100, 200)          0         
_________________________________________________________________
bidirectional_9 (Bidirection (None, 100, 100)          100400    
_________________________________________________________________
dropout_9 (Dropout)          (None, 100, 100)          0         
_________________________________________________________________
bidirectional_10 (Bidirectio (None, 100, 60)           31440     
_________________________________________________________________
dropout_10 (Dropout)         (None, 100, 60)           0         
_________________________________________________________________
bidirectional_11 (Bidirectio (None, 20)                5680      
_________________________________________________________________
dropout_11 (Dropout)         (None, 20)                0         
_________________________________________________________________
dense_2 (Dense)              (None, 1)                 21        
=================================================================
Total params: 221,541
Trainable params: 221,541
Non-trainable params: 0
_________________________________________________________________
None
STEPS:  100
FORECAST:  1
Epoch 1/12
 - 3082s - loss: 0.0029
Epoch 2/12
 - 3078s - loss: 0.0016
Epoch 3/12
 - 3077s - loss: 0.0014
Epoch 4/12
 - 3078s - loss: 0.0013
Epoch 5/12
 - 3078s - loss: 0.0012
Epoch 6/12
 - 3078s - loss: 0.0012
Epoch 7/12
 - 3078s - loss: 0.0012
Epoch 8/12
 - 3078s - loss: 0.0012
Epoch 9/12
 - 3077s - loss: 0.0011
Epoch 10/12
 - 3079s - loss: 0.0010
Epoch 11/12
 - 3078s - loss: 9.9018e-04
Epoch 12/12
 - 3078s - loss: 9.7333e-04
MODELO SALVO
Nome do Modelo:  Modelos_Tensao/modelo_bidirecional_radiacao_wavelet_multvariado_camada_3_lendo_100_predicao_1

ERROS:
Score Variance: 0.996 
Erro Absoluto Médio: 13.438 
Erro Médio Quadrático: 451.754 
MAPE: 111864825.000 
RMSE: 21.255 
Erro Mediano Absoluto: 9.728 
Erro R2: 0.996
